
var lineDrawing = anime({
    targets: '#lineDrawing .lines path',
    strokeDashoffset: [anime.setDashoffset, 0],
    easing: 'easeInOutSine',
    duration: 2500,
    delay: function (el, i) { return i * 250 },
    direction: 'alternate',
    loop: false
});

function isElementInViewport(elem) {
    var elementTop = $(elem).offset().top;
    var elementBottom = elementTop + $(elem).outerHeight();
    var viewportTop = $(window).scrollTop();
    var viewportBottom = viewportTop + $(window).height();
    return elementBottom > viewportTop && elementTop < viewportBottom;
}

function checkAnimation() {
    var $elem = $(".animatee");
    //     // Check if it's time to start the animation.
    if (isElementInViewport($elem)) {
        // Start the animation
        $(".animateIn").slideDown(1000);
    }
}

// $quote = $('.coachQuote')
// if (isElementInViewport($quote)){
    var options = {
        strings: ['" Feghas Solutions is one of the companies I coached and helped execute their Stanford Seed Transformation Plan. With a dynamic and experienced leadership team, they offers innovative digital solutions for education, government, financial and SME sectors. At Feghas, customers are number one priority, they employs a strong marketing operation to better know and serve its customers while reinforcing its technical and development abilities to better support its customer base "'],
        typeSpeed: 20,
        loop: false,
        showCursor: true,
        cursorChar: '|',
        autoInsertCss: true
    }
    var typed = new Typed(".element", options);
// }

$(window).scroll(function () {
    checkAnimation();
});

function initMap() {
    var uluru = { lat: 6.6344424, lng: 3.3592129 };
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 10,
        center: uluru
    });
    var marker = new google.maps.Marker({
        position: uluru,
        map: map
    });
}