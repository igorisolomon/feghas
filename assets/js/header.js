"use strict";

//Get the header
var header = document.getElementById("header");

/**
 * Creating the container
 * Giving it class
 * Appending it to the parent header
*/
var container = document.createElement("div");
container.className = "container";
header.appendChild(container);

//#region LOGO
/**
 * create the element
 * give it a class
 * Append to its parent
 * add other attribute
 */
var logo = document.createElement("div");
logo.className = "logo";
container.appendChild(logo);

var brand = document.createElement("a");
brand.setAttribute("href","index.html");
logo.appendChild(brand);

var img = document.createElement("img");
img.setAttribute("src","logo.png");
img.setAttribute("alt", "feghas logo");
brand.appendChild(img);
//#endregion

//#region NAV
/**
 * create the element
 * give it a class
 * append it to its parent
 * add other attribute
 */
var nav = document.createElement("nav");
nav.className = "main-nav navbar navbar-right navbar-expand-md";
container.appendChild(nav);

//button (Create, Class, Append)
var button = document.createElement("button");
button.id = "navbar-collapse";
//button.className = "navbar-toggler collapsed";
button.setAttribute("type","button");
button.setAttribute("data-toggle","collapse");
button.setAttribute("data-target","#navbar-collapse");
nav.appendChild(button);

//creating span
var i;
var span;
for(i=0; i<3; i++){
    span[i] = document.createElement("span");
    button.appendChild(span[i]);
}

//#endregion